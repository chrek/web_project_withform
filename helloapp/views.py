from django.shortcuts import render, redirect
import getpass

from datetime import datetime
from django.http import HttpResponse
from django.views.generic import ListView

from helloapp.forms import LogMessageForm
from helloapp.models import LogMessage

# Create your views here.
username = getpass.getuser()
""" def home(request):
    #return HttpResponse("Welcome, Django!")
    return render(request, "helloapp/home.html") """

class HomeListView(ListView):
    """Renders the home page, with a list of all messages."""
    model = LogMessage

    def get_context_data(self, **kwargs):
        context = super(HomeListView, self).get_context_data(**kwargs)
        return context

def about(request):
    return render(request, "helloapp/about.html")

def contact(request):
    return render(request, "helloapp/contact.html")

def hello_friend(request, name):
    return render(
        request,
        'helloapp/hello_friend.html',
        {
            'name': name,
            'date': datetime.now()
        }
    )

# Add this code elsewhere in the file:
def log_message(request):
    form = LogMessageForm(request.POST or None)

    if request.method == "POST":
        if form.is_valid():
            message = form.save(commit=False)
            message.log_date = datetime.now()
            message.save()
            return redirect("home")
    else:
        return render(request, "helloapp/log_message.html", {"form": form})