**This is a Django Project with a "helloapp" app**

This Django project web_project has just one app helloapp. 
The app is configured to serve static files and has four pages (Home, About, Contact and Log Message) that use a common base template.
Each page contains a nav bar from a base template. 

The form page is used to log messages which are later displayed on the home page. 

A default SQLite database is created in the file db.sqlite3 when you run the server the first time. 
This database is intended for development purposes or for low-volume web apps in production. 

Run the app on the Django's built-in web server from the command line using `python manage.py runserver`.

Use the browser to access the app http://127.0.0.1:8000/


**Templates**

The application has different views. A view is a "type" of web page for a specific function and is tied to a specific 
template. A template is an HTML (markup) file that can be used to provide placeholders for data values that the code provides at run time. 

**Template inheritance**

You can define a base page with common markup and then build upon that base with page-specific additions.

**Django Form**

The form contains field drawn from the data model - LogMessage. Data is stored and retrieved from the model.

## References

1. [Code.VisualStudio.com](https://code.visualstudio.com/docs/python/tutorial-django/)
2. [Docs.djangoproject](https://docs.djangoproject.com/en/2.1/intro/tutorial03/)